import logging
import telegram
from flask import Flask, request
from telegram import Bot, Update
from telegram.ext import Updater,  MessageHandler

#from telegram import  NetworkError
TOKEN = 'YOUR_KEY'
MYSERVICEURL = 'https://6dcd-2a02-aw12-e045-e80-bb3-s3a1-448d-57da.eu.ngrok.io'
app = Flask(__name__)


# Set up logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)

# Create a bot object to handle incoming updates using the Telegram API
bot = telegram.Bot(token= TOKEN)

# Define a route to handle incoming webhook updates
@app.route('/webhook', methods=['POST'])
async def webhook():
    update = telegram.Update.de_json(request.get_json(force=True), bot)
    try:
        # Call the handler function with the received update
        await handle_update(update)
    except Exception as e:
        logger.error(f"Exception while handling update: {e}")
    return 'OK'

# Define a handler function for incoming updates
async def handle_update(update):
    if update.message is not None:
        # Handle text messages
        text = update.message.text
        chat_id = update.message.chat_id
        await bot.send_message(chat_id=chat_id, text=f"You said: {text}")

@app.route('/setwebhook', methods=['GET', 'POST'])
async def handle_hooksetup():
    webhook_url = MYSERVICEURL + '/webhook'
    await bot.delete_webhook()
    s = await bot.setWebhook(url=webhook_url)
    if s:
        return "webhook setup ok" + webhook_url+'/' + TOKEN
    else:
        return "webhook setup failed"

               
if __name__ == '__main__':
    # Set up the webhook
    #handle_hooksetup()
    
    # Start the Flask app
    app.run(port=5000, debug=True)

